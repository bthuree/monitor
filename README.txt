This is a set of checks that will print out OK or NOK to both console as well as a log file.

An sample run looks like this (only displaying NOK)

bash-3.2# /JUMP/scripts/slask/monitor/bin/main.bash -f
20171109111006:NOK:SIT1_site0:Xnix:Hard disk issues:emoam02e2:Hard disk issues on c0t1d0 error count 0 4 0 4
20171109111036:NOK:SIT1_site0:Xnix:File system full:emmws1:File System getting full on /dev/dsk/c0t0d0s7
20171109111055:NOK:SIT1_site0:Xnix:Core files:emmws1:Core File Found (/core)
20171109111703:NOK:SIT1_site0:Xnix:HW Msg:emmws1:Check /var/adm/messages file. Possible HW Issues
20171109112106:NOK:SIT1_site0:ldap:password_no_expiry:emadm1:LDAP password aging indetermined for 192.168.51.7
20171109114957:NOK:SIT1_site0:ldap:replication:emoam02:LDAP Replication: Missmatch between number of users in LDAP
20171109115259:NOK:SIT1_site0:ombs:all_policies_active:emomb1:Policy (emuas2-bkp_FILES) disabled
20171109115333:NOK:SIT1_site0:solaris:metadevice_issues:emmws1:Disksuite: Inactive metadb(s) found
20171109115404:NOK:SIT1_site0:solaris:svcs:emoam02:To many ( 2) offlined processes
20171109115427:NOK:SIT1_site0:EndSummary:EndSummary:emmws1:9 Failed and 274 passed


Options: -v, --version                  print version information and exit
        --versionnumber                 print ONLY version number and exit
         -h, --help                     print this help and exit
         -f, --fail                     Only display failed results and summary
         -a, --all                      Execute all checks (default)
         -d, --dir <Script Directory>   Execute all checks in <Script Dir>
         -c, --cmd <script>				Only execute <script>. Valid with -d parameter
             --testing					Only execute test scripts in script dir testing. 
             							(these scripts will not be run in normal run)


