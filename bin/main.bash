#!/bin/bash

## Main Monitor script to be run from the MWS
## 
## Will loop through server type directories and execute each script there.
##  If a script name ends with .NO, it will not be executed.

##	scripts
## 		script_type
##				<scripts to be run>

MONITOR_VERSION=0.1

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../lib/common.bash

_ALL_SCRIPTS="YES"
_SHOW_NOK_ONLY="NO"
_SCRIPT=""
_SCRIPT_DIR=""
_TESTING_RUN="NO"
_TESTDIR="testing"
__OK_TEST=0
__NOK_TEST=0

usage() {
        echo ""
        if [ "$1" = -h ]
        then
                exec 1>&2
                echo "U\c"
        else
                echo "u\c"
        fi

        echo "sage:     `basename $0`  -b <base file> [ options ] "
        echo ""
    
        echo "`basename $0` is a tool to help out with automatic checking of the various OSS-RC and ENIQ components"


        cat <<EOCAT

Options: -v, --version                  print version information and exit
        --versionnumber                 print ONLY version number and exit
         -h, --help                     print this help and exit
         -f, --fail                     Only display failed results and summary
         -a, --all                      Execute all checks (default)
         -d, --dir <Script Directory>   Execute all checks in <Script Dir>
         -c, --cmd <script>				Only execute <script>. Valid with -d parameter
             --testing					Only execute test scripts in script dir testing. 
             							(these scripts will not be run in normal run)
EOCAT

        echo "Sample:"
        echo "  `basename $0` "
        echo "  `basename $0` -d solaris"
        echo "  `basename $0` -d ombs -c backup_errors.bash "

        echo ""
        exit 0
}


Check_Cmd_Line() {

        { # Check command line parameters       while [ $# -ne 0 ]
        while [ $# -ne 0 ]
        do
                case "$1" in
                        -v|--version)
                                echo ""
                                echo "`basename $0` version ${CHECK_BASELINE_VERSION}" \
                                "by Bengt Thuree"
                                echo ""
                                exit 0
                                ;;
                        --versionnumber)
                                echo "${CHECK_BASELINE_VERSION}"
                                exit 0
                                ;;
                        -h|--help)
                                usage -h
                                ;;
                        -a|--all)
                                _ALL_SCRIPTS="YES"
                                ;;
                        -f|--fail)
                                _SHOW_NOK_ONLY="YES"
                                ;;
                        -d|--dir)
                                shift
                                _ALL_SCRIPTS="NO"                              
                                _SCRIPT_DIR="$1"
                                ;;
                        -c|--cmd)
                                shift
                                _ALL_SCRIPTS="NO"                                                              
                                _SCRIPT="$1"
                                ;;
                        --testing)
                                _ALL_SCRIPTS="NO"                                                              
                                _TESTING_RUN="YES"
                                ;;
                        *)
                                echo ""
                                echo "`basename $0`: UNKNOWN COMMAND/OPTION" 2>&1
                                echo ""
                                usage
                                exit 1
                                ;;
                esac
                shift
        done
        
        if [ "x${_SCRIPT}" != "x" ] && [ "x${_SCRIPT_DIR}" == "x" ]; then
                echo ""
                echo "${BASENAME}: Script Directory has to be specified with Script name" 2>&1
                echo ""
                usage
                exit 1
        fi
        
        } # End checking command line parameters
}



main() {
	SCRIPTDIR=`find ${_MYPATH}/../scripts/* -type d -prune`
	for _dir in ${SCRIPTDIR}
	do
		if [ "x${_SCRIPT_DIR}" == "x`basename ${_dir}`" ] || 
			( [ "x${_ALL_SCRIPTS}" == "xYES" ] && [ "x${_TESTDIR}" != "x`basename ${_dir}`" ] ) ||
			( [ "x${_TESTING_RUN}" == "xYES" ] && [ "x${_TESTDIR}" == "x`basename ${_dir}`" ] ); then
			SCRIPTS=`find ${_dir} -type f -perm -u+rx`
			for _script in ${SCRIPTS}
			do
				if [ ${_script: -3} != ".NO" ]; then 
					if [ "x${_SCRIPT}" == "x`basename ${_script}`" ] || [ "x${_SCRIPT}" == "x" ]; then
						${_script}
					fi
				fi
			done
		fi
	done
}

PREPARE

Check_Cmd_Line $*
if [ "${_SHOW_NOK_ONLY}" == "YES" ]; then
    SET_SHOW_NOK_ONLY
fi

SELECT_ENV

main

CHECK="EndSummary"
CHECKTYPE="EndSummary"
PRINTSUMMARY

CLEAN_UP_FILES
