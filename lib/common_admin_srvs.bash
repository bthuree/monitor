#!/bin/bash

## Common functions for Master server scripts.

GEO_OSS_PROCESS="ClusterService"

IS_ADMIN_SRV () {
        RESULT="NO"
        TYPE=MS
        SELECT_ENV
        SERVERS=`SELECT_SERVER`
        for _TMP_SRV in ${SERVERS}
        do
                if [ "${_TMP_SRV}" == "${SERVER}" ]; then
                        RESULT="YES" 
                        break
                fi
        done
        echo "${RESULT}"
}

IS_GEO_CLUSTER() {
        TYPE=MS
        SELECT_ENV
        SERVERS=`SELECT_SERVER`
        for SERVER in ${SERVERS}
        do
                PING || continue
                COMMAND="/opt/VRTSvcs/bin/hastatus -sum"
                _OUTPUT="`SSH_SERVER_ERR_2_NULL`"

                _GEO_DEF=$(echo "$_OUTPUT" | grep "^B" | grep $GEO_OSS_PROCESS)

                if [ "x${_GEO_DEF}" != "x" ]; then 
                        return 0
                fi
                break
        done
        return 1
}

PASSIVE_SIDE_GEO_CLUSTER() {
        TYPE=MS
        SELECT_ENV
        SERVERS=`SELECT_SERVER`
        for SERVER in ${SERVERS}
        do
                PING || continue
                COMMAND="/opt/VRTSvcs/bin/hastatus -sum"
                _OUTPUT="`SSH_SERVER_ERR_2_NULL`"

                _GEO_DEF=$(echo "$_OUTPUT" | grep "^B" | grep $GEO_OSS_PROCESS)

                _REMOTE_SYB_OK=$(echo "$_OUTPUT" | grep "^P" | grep ONLINE | awk '{print $2}' | grep "Sybase1$")
                _REMOTE_OSS_OK=$(echo "$_OUTPUT" | grep "^P" | grep ONLINE | awk '{print $2}' | grep "Oss$")

                if [ "x${_GEO_DEF}" != "x" ]; then 
                        if [ "x${_REMOTE_OSS_OK}" != "x" ] || [ "x${_REMOTE_SYB_OK}" != "x" ]; then
                                return 0
                        fi
                        return 1
                fi
                break
        done
        return 1
}

ACTIVE_SIDE_GEO_CLUSTER() {
        TYPE=MS
        SELECT_ENV
        SERVERS=`SELECT_SERVER`
        for SERVER in ${SERVERS}
        do
                PING || continue
                COMMAND="/opt/VRTSvcs/bin/hastatus -sum"
                _OUTPUT="`SSH_SERVER_ERR_2_NULL`"

                _GEO_DEF=$(echo "$_OUTPUT" | grep "^B" | grep $GEO_OSS_PROCESS)

                _SYB_OK=$(echo "$_OUTPUT" | grep "^B" | grep ONLINE | awk '{print $2}' | grep "Sybase1$")
                _OSS_OK=$(echo "$_OUTPUT" | grep "^B" | grep ONLINE | awk '{print $2}' | grep "Oss$")


                if [ "x${_GEO_DEF}" != "x" ]; then 
                        if [ "x${_OSS_OK}" != "x" ] || [ "x${_SYB_OK}" != "x" ]; then
                                return 0
                        fi
                        return 1
                fi
                break
        done
        return 1
}

GET_OSS_SERVER () {
        TYPE=MS
        SELECT_ENV
        SERVERS=`SELECT_SERVER`
        for SERVER in ${SERVERS}
        do
                PING || continue
                COMMAND="/opt/VRTSvcs/bin/hagrp -state -localclus Oss"
                _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
                echo "$_OUTPUT" | nawk '/ONLINE/{print $3}'
                break
        done
        echo ""
}

GET_SYBASE_SERVER () {
        TYPE=MS
        SELECT_ENV
        SERVERS=`SELECT_SERVER`
        for SERVER in ${SERVERS}
        do
                PING || continue
                COMMAND="/opt/VRTSvcs/bin/hagrp -state -localclus Sybase1"
                _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
                echo "$_OUTPUT" | nawk '/ONLINE/{print $3}'
                break
        done
        echo ""
}

IS_ADMIN_SERVER () {
        TYPE="MS"
        MS_SERVERS=`SELECT_SERVER`

        if [[ ${MS_SERVERS} =~ "${SERVER}" ]]; then
                return 0
        else
                return 1
        fi
}