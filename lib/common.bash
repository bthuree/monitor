#!/bin/bash

ENV_FILE1="${_MYPATH}/../etc/env.dat"
ENV_FILE2="${_MYPATH}/../../etc/env.dat"
if [ -f ${ENV_FILE1} ]; then
    ENV_FILE=${ENV_FILE1}
else
    ENV_FILE=${ENV_FILE2}
fi

RESULT_SUMMARY_FILE=/tmp/Monitoring.RESULT_SUMMARY_FILE.txt
SHOW_NOK_ONLY=/tmp/Monitoring.SHOW_NOK_ONLY
_YEAR=`date +"%Y"`
_MONTH=`date +"%m"`
RESULT_DIR=/JUMP/backups/Monitoring/Results/
RESULT_FILE=${RESULT_DIR}/${_YEAR}${_MONTH}.log
LAST_RESULT_FILE=${RESULT_DIR}/LAST_RUN.log


CLEAN_UP_FILES(){
    rm -f ${RESULT_SUMMARY_FILE}
    rm -f ${SHOW_NOK_ONLY}
}

PREPARE() {
    CLEAN_UP_FILES
    mkdir -p ${RESULT_DIR}
    # Ensure we remove the last result file
    rm -f ${LAST_RESULT_FILE}

}


SET_SHOW_NOK_ONLY () {
    touch ${SHOW_NOK_ONLY}
}

PRINTSUMMARY() {
    _OK=0
    _NOK=0
    if [ -f ${RESULT_SUMMARY_FILE} ] ; then     
        _OK=`cat ${RESULT_SUMMARY_FILE} | awk '{print $1}'`
        _NOK=`cat ${RESULT_SUMMARY_FILE} | awk '{print $2}'`
    fi
    if [ ${_NOK} -gt 0 ]; then
        LOG_FAIL_RESULT "${_NOK} Failed and ${_OK} passed"
    else
        LOG_OK_RESULT "${_OK} passed"
    fi
}


UPDATE_RESULT_SUMMARY_FILE() {
    _OK=0
    _NOK=0
    if [ -f ${RESULT_SUMMARY_FILE} ] ; then     
        _OK=`cat ${RESULT_SUMMARY_FILE} | awk '{print $1}'`
        _NOK=`cat ${RESULT_SUMMARY_FILE} | awk '{print $2}'`
    fi
    if [ ${CHKRESULT} == "NOK" ]; then
        _NOK=`expr ${_NOK} + 1`
    else
        _OK=`expr ${_OK} + 1`
    fi
    echo "${_OK}    ${_NOK}" > ${RESULT_SUMMARY_FILE}
}

TIMESTAMP_30_minutes_ago() {
    perl -e 'use POSIX qw(strftime); print strftime("%Y%m%d%H%M\n", localtime(time-1800));'
}

TIMESTAMP_15_minutes_ago() {
    perl -e 'use POSIX qw(strftime); print strftime("%Y%m%d%H%M\n", localtime(time-900));'
}


SELECT_ENV () {
        host=`hostname| awk -F. '{ print $1}'`
        ENV=`cat ${ENV_FILE} | egrep -v "^#|^$" | grep $host | awk '{ print $1 }'|grep -v "^$"`
}

SELECT_SERVER () {
        ## Need to be able to filter on beginning of TYPE, so you get UAS1 and UAS2 when you select type UAS.
        cat ${ENV_FILE} | egrep -v "^#|^$" | grep $ENV | 
                awk ' /'$TYPE'/ {print $5;}'
}

SERVER_NAME () {    
    ## Need to be able to filter excactly on server IP (so you get .4 but not .41)
    if [ "x${SERVER}" == "x" ]; then
        ## Set SERVER to current hosts main IP (search host file for hostname, and remove all hostname-)
        _H=`hostname`
        SERVER=`grep ${_H} /etc/hosts | grep -v "${_H}-" |awk '{print $1}'`
    fi
    if [ "x${ENV}" == "x" ]; then
        SELECT_ENV
    fi
    cat ${ENV_FILE} | egrep -v "^#|^$" | grep $ENV | awk '{ if ($5=="'$SERVER'") print $4}' 
}

GET_IP_FOR_NAME() {
    NAME=$1
    cat ${ENV_FILE} | egrep -v "^#|^$" | grep $ENV | awk '{ if ($4=="'$NAME'") print $5}' 
}

NEDSS_NAME () { 
        cat ${ENV_FILE} | egrep -v "^#|^$" | grep $ENV | 
                awk '{ if ($2=="NEDSS") print $4}' 

}

SERVER_OS () {
        cat ${ENV_FILE} | egrep -v "^#|^$" | grep $ENV | 
                awk '{ if ($5=="'$SERVER'") print $3}' 
}

SERVER_TYPE () {
        cat ${ENV_FILE} | egrep -v "^#|^$" | grep $ENV | 
                awk '{ if ($5=="'$SERVER'") print $2}' 
}

PING () {
        _TMP_OUTPUT="`ping $SERVER 5 2>/dev/null `"
        return $?
}

LINUX () {
        if [ "`SERVER_OS`" == "linux" ]
        then
          return 0
        fi
        return 1
}

ENIQ () {
        if [ "`SERVER_TYPE`" == "ENIQ" ]
        then
          return 0
        fi
        return 1
}
UAS () {
        if [ "`SERVER_TYPE`" == "UAS1" ]  || [ "`SERVER_TYPE`" == "UAS2" ]
        then
          return 0
        fi
        return 1
}

SSH_SERVER () {
#       PASSWORD=`grep $SERVER /scripts/passwd/${ENV}|awk -F: '{ print $2 }'`
#       ../lib/ssh.exp root $SERVER $PASSWORD ${COMMAND}|sed '/^$/d'
        if [ "x$1" == "x" ]; then
            ssh ${SERVER} "${COMMAND}" | sed '/^$/d'
        else
            ssh ${SERVER} -l "$1" "${COMMAND}" | sed '/^$/d'
        fi

}

SSH_SERVER_ERR_2_NULL () {
#       PASSWORD=`grep $SERVER /scripts/passwd/${ENV}|awk -F: '{ print $2 }'`
#       ../lib/ssh.exp root $SERVER $PASSWORD ${COMMAND}|sed '/^$/d'
        if [ "x$1" == "x" ]; then
            ssh ${SERVER} "${COMMAND}" 2>/dev/null | sed '/^$/d'
        else
            ssh ${SERVER} -l "$1" "${COMMAND}" 2>/dev/null | sed '/^$/d'
        fi
}

LOG_RESULT() {
        YEAR=`date +"%Y"`
        MONTH=`date +"%m"`
        DAY=`date +"%d"`
        HOUR=`date +"%H"`
        MINUTE=`date +"%M"`
        SECOND=`date +"%S"`

        if [ "a${CHECKTYPE}" == "a" ]
        then
                ## Use the script dirname as checktype
                CHECKTYPE=`dirname $0`
                CHECKTYPE=`basename ${CHECKTYPE}`
        fi


        if [ "a${CHECK}" == "a" ]
        then
                ## use the script as a check name
                CHECK=`basename $0`
                CHECK="${CHECK%.*}"
        fi

        TIMESTAMP=${YEAR}${MONTH}${DAY}${HOUR}${MINUTE}${SECOND}
        DESCR="$@"

        if ([ -f ${SHOW_NOK_ONLY} ] && [ "${CHKRESULT}" == "NOK" ]) || [ ! -f ${SHOW_NOK_ONLY} ] || [ "${CHECKTYPE}" == "EndSummary" ]; then
            printf "${TIMESTAMP}:${CHKRESULT}:${ENV}:${CHECKTYPE}:${CHECK}:`SERVER_NAME`:${DESCR}\n"
        fi
        ## Log all to result file also
        printf "${TIMESTAMP}:${CHKRESULT}:${ENV}:${CHECKTYPE}:${CHECK}:`SERVER_NAME`:${DESCR}\n" >> ${RESULT_FILE}
        ## And lastly log same to last result file
        printf "${TIMESTAMP}:${CHKRESULT}:${ENV}:${CHECKTYPE}:${CHECK}:`SERVER_NAME`:${DESCR}\n" >> ${LAST_RESULT_FILE}

        UPDATE_RESULT_SUMMARY_FILE "${CHKRESULT}"
}

LOG_FAIL_RESULT() {
        CHKRESULT="NOK"
        DESCR="$@"
        LOG_RESULT ${DESCR}
}

LOG_OK_RESULT() {
        CHKRESULT="OK "
        DESCR="$@"
        LOG_RESULT ${DESCR}
}
