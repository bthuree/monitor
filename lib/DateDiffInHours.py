#!/usr/bin/python

## Small extra library

import sys
import datetime

#print ("Number of arguments %s" % len (sys.argv))

year_1	= int(sys.argv[1])
month_1	= int(sys.argv[2])
day_1	= int(sys.argv[3])
hour_1	= int(sys.argv[4])
min_1	= int(sys.argv[5])
d1 = datetime.date (year_1, month_1, day_1)
t1 = datetime.time (hour_1, min_1)
dt1 = datetime.datetime.combine(d1, t1)

if (len (sys.argv) > 6):
	year_2	= int(sys.argv[6])
	month_2	= int(sys.argv[7])
	day_2	= int(sys.argv[8])
	hour_2	= int(sys.argv[9])
	min_2	= int(sys.argv[10])
	d2 = datetime.date (year_2, month_2, day_2)
	t2 = datetime.time (hour_2, min_2)
	dt2 = datetime.datetime.combine(d2, t2)
else:
	dt2 = datetime.datetime.now()

#print "Date 1 ", dt1
#print "Date 2 ", dt2

dt3 = dt2 - dt1

#print "Diff  ", dt3

time_diff_hours = dt3.seconds // 3600 + dt3.days * 24
#print "Diff hours ", time_diff_hours
print time_diff_hours