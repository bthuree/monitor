#!/bin/bash

#OAM Check

# show enclosure status
# Enclosure:
#         Status: OK
#         Unit Identification LED: Off
#         Diagnostic Status:
#                 Internal Data                            OK
#                 Redundancy                               OK
#                 Location Services                        OK

# Onboard Administrator:
#         Status: OK

# Power Subsystem:
#         Status: OK
#         Power Mode: Redundant
#         Redundancy State: Redundant
#         Power Capacity: 6750 Watts DC
#         Power Available: 3861 Watts DC
#         Present Power: 1328 Watts DC

# Cooling Subsystem:
#         Status: OK
#         Fans Good/Wanted/Needed: 10/10/9
#         Fan 1:  5501 RPM (31%)
#         Fan 2:  5500 RPM (31%)
#         Fan 3:  6650 RPM (37%)
#         Fan 4:  6651 RPM (37%)
#         Fan 5:  6650 RPM (37%)
#         Fan 6:  6620 RPM (37%)
#         Fan 7:  6619 RPM (37%)
#         Fan 8:  6650 RPM (37%)
#         Fan 9:  6652 RPM (37%)
#         Fan 10:  6651 RPM (37%)
