#!/bin/bash

ldap_search2() {
	TMP_RESULT=OK
	if [ ${_LDAP_CLIENTS} -ne 0 ]; then

		COMMAND='ldapclient list|grep NS_LDAP_SERVERS'
		_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
		_client_list=$(echo ${_OUTPUT} | awk -F= '{print $2}' | sed 's/,/ /g' )

		for _INF_SERVER in $_client_list
		do
			COMMAND='ldapsearch -h '${_INF_SERVER}' -P /var/ldap/  -b "" -s base "objectclass=*" namingcontexts'
			_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
			_res=$(echo "${_OUTPUT}" | grep namingcontexts | head -1  | awk '{print $2}')
			if [ "x${_res}" != "xdc=globaldomain" ]; then
				LOG_FAIL_RESULT "LDAP search failed towards ${_INF_SERVER}"
				TMP_RESULT=NOK
			fi	
		done
		if [ ${TMP_RESULT} == "OK" ]; then
			LOG_OK_RESULT "LDAP search ok"
		fi
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

_LDAP_CLIENTS=2

if `IS_GEO_CLUSTER`; then
        if `ACTIVE_SIDE_GEO_CLUSTER`; then
                _LDAP_CLIENTS=2
        else
                _LDAP_CLIENTS=0
        fi
else
		_LDAP_CLIENTS=2
fi

TYPE="UAS|MS"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        ldap_search2
done
