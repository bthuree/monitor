#!/bin/bash

LDAP_TMP_FILE=/tmp/_LDAP_TMP_FILE.$$
LDAPPASSWORD=ldapadmin

ldap_rep_1 () {
	COMMAND='ldapsearch -D "cn=Directory Manager" -w '${LDAPPASSWORD}' -b "" -s base "objectclass=*" namingcontexts'
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_ldap_domain=$(echo "${_OUTPUT}" | grep namingcontexts | tail -1  | awk '{print $2}')

	TMP_RESULT=OK
	COMMAND="egrep RESULT.*cn=replication.manager /var/ds/logs/access| tail -10"
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_res=$(echo ${_OUTPUT} | grep RESULT | grep -v err=0 | wc -l)
	if [ ${_res} -ne 0 ]; then
		LOG_FAIL_RESULT "LDAP Replication: Replication Errors found"
		TMP_RESULT=NOK
	fi
	COMMAND='ldapsearch -T -D "cn=directory manager" -w '${LDAPPASSWORD}' -b "uid=nmsadm,ou=people,'${_ldap_domain}'" "objectclass=*"'
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_res=$(echo ${_OUTPUT} | grep dn: | wc -l)
	if [ ${_res} -ne 1 ]; then
		LOG_FAIL_RESULT "LDAP Replication: User nmsadm not found on ${SERVER}"
		TMP_RESULT=NOK
	fi

	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "LDAP Replication: No Replication Errors found"
	fi
}

ldap_rep_2_pre () {
	COMMAND='ldapsearch -D "cn=Directory Manager" -w '${LDAPPASSWORD}' -b "" -s base "objectclass=*" namingcontexts'
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_ldap_domain=$(echo "${_OUTPUT}" | grep namingcontexts | tail -1  | awk '{print $2}')

	COMMAND='ldapsearch -T -D "cn=directory manager" -w '${LDAPPASSWORD}' -b "uid=nmsadm,ou=people,'${_ldap_domain}'" "objectclass=*"'
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	echo "${_OUTPUT}" | grep dn: | wc -l >> ${LDAP_TMP_FILE}
}


ldap_rep_2_post () {
	TMP_RESULT=OK
	_old_res=0
	{
		while read _new_res; do
			if [ ${_old_res} != ${_new_res} ] && [ ${_old_res} -ne 0 ]; then
				LOG_FAIL_RESULT "LDAP Replication: Missmatch between number of users in LDAP"
				TMP_RESULT=NOK
			fi
			_old_res=${_new_res}
		done < ${LDAP_TMP_FILE}
	}
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "LDAP Replication: Same number of users on each ldap server"
	fi
	rm ${LDAP_TMP_FILE}
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

CHECK="replication"
TYPE="INF"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
	PING || continue
	ldap_rep_1
	ldap_rep_2_pre
done
ldap_rep_2_post
