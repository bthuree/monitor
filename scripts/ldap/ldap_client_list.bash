#!/bin/bash

ldap_client_list () {
	if [ ${_LDAP_CLIENTS} -ne 0 ]; then
		TMP_RESULT=OK
		_NBR_CLIENTS=$1
		COMMAND='ldapclient list|grep NS_LDAP_SERVERS'
		_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
		_res=$(echo ${_OUTPUT} | awk -F= '{print $2}' |  wc -w)
		if [ ${_res} -ge 1 ]; then
			LOG_OK_RESULT "LDAP Client List ok"
			TMP_RESULT=OK
		else	
			LOG_FAIL_RESULT "LDAP Client List ${_res} not ${_NBR_CLIENTS}"
			TMP_RESULT=NOK		
		fi
	fi
}

ldap_list () {
	if [ ${_LDAP_CLIENTS} -ne 0 ]; then
		TMP_RESULT=OK
		COMMAND="ldaplist ou=hosts | grep ${SERVER}"
		_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
		_res=$(echo ${_OUTPUT} | grep "dn: cn" | wc -l)
		if [ ${_res} -eq 0 ]; then
			LOG_FAIL_RESULT "LDAP List failed"
			TMP_RESULT=NOK
		fi
		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "LDAP List ok"
		fi
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

_LDAP_CLIENTS=2

if `IS_GEO_CLUSTER`; then
        if `ACTIVE_SIDE_GEO_CLUSTER`; then
                _LDAP_CLIENTS=2
        else
                _LDAP_CLIENTS=0
        fi
else
		_LDAP_CLIENTS=2
fi

TYPE="UAS|MS"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
    PING || continue
#    ldap_client_list ${_LDAP_CLIENTS}
    ldap_client_list
	ldap_list     
done


