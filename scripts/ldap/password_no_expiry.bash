#!/bin/bash

LDAPPASSWORD=ldapadmin

password_check_no_expiry() {
	TMP_RESULT=OK
	COMMAND='ldapclient list|grep NS_LDAP_SERVERS'
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_client_list=$(echo ${_OUTPUT} | awk -F= '{print $2}' | sed 's/,/ /g' )

	for _INF_SERVER in $_client_list
	do
		COMMAND='ldapsearch -h '${_INF_SERVER}' -P /var/ldap/  -b "" -s base "objectclass=*" namingcontexts'
		_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
		_ldap_domain=$(echo "${_OUTPUT}" | grep namingcontexts | tail -1  | awk '{print $2}')
		if [ "x${_ldap_domain}"  == "x" ]; then
			LOG_FAIL_RESULT "LDAP password aging indetermined for ${SERVER}"
			TMP_RESULT=NOK
		else 
			COMMAND='ldapsearch -h '${_INF_SERVER}' -T -p 389 -D "cn=Directory Manager" -w '${LDAPPASSWORD}' -b "cn=securitypolicy,'${_ldap_domain}'" "objectclass=ldapsubentry"'
			_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
			_pwdMaxAge=$(echo "${_OUTPUT}" | grep ^pwdMaxAge | awk '{print $2}')
			_passwordMustChange=$(echo "${_OUTPUT}" | grep ^passwordMustChange | awk '{print $2}')
			if [ ${_pwdMaxAge} -eq 0 ] && [ "${_passwordMustChange}" == "off" ]; then
				TMP_RESULT=OK
			else
				LOG_FAIL_RESULT "LDAP password aging active ${SERVER}"
				TMP_RESULT=NOK
			fi
		fi
	done
	if [ ${TMP_RESULT} == "OK" ]; then
		LOG_OK_RESULT "LDAP password aging disabled"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

_LDAP_CLIENTS=2

if `IS_GEO_CLUSTER`; then
        if `ACTIVE_SIDE_GEO_CLUSTER`; then
                _LDAP_CLIENTS=2
        else
                _LDAP_CLIENTS=0
        fi
else
		_LDAP_CLIENTS=2
fi

if [ ${_LDAP_CLIENTS} -ne 0 ]; then

	TYPE="MS1"
	SELECT_ENV

	SERVERS=`SELECT_SERVER`
	for SERVER in ${SERVERS}
	do
	        PING || continue
	        password_check_no_expiry
	done
fi