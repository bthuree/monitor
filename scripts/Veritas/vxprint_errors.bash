#!/bin/bash

vxprint_errors () {
        TMP_RESULT=OK                
        COMMAND="vxprint"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _vxprint_errors=$(echo "$_OUTPUT" | egrep -i "iofail | recover")

        if [ "x${_vxprint_errors}" != "x" ]; then
                LOG_FAIL_RESULT "vxprint gave errors : ${_vxprint_errors}"
                TMP_RESULT=NOK                
        fi
        if [ "${TMP_RESULT}" == "OK" ]; then
                LOG_OK_RESULT "vxprint indicates no errors"
        fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

TYPE="MS|NAS"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        vxprint_errors
done



