#!/bin/bash

HALOG=/var/VRTSvcs/log/engine_A.log

DAY=`date +'%d'`
MONTH=`date +'%m'`
YEAR=`date +'%Y'`

TODAY=${YEAR}/${MONTH}/${DAY}

TODAY_MINUS_1=`perl -e 'use POSIX qw/strftime/; print strftime ("%Y %m %d\n", localtime( time - 86400 ));'| awk '{print $1"/"$2"/"$3}'`
TODAY_MINUS_2=`perl -e 'use POSIX qw/strftime/; print strftime ("%Y %m %d\n", localtime( time - 172800 ));'| awk '{print $1"/"$2"/"$3}'`
TODAY_MINUS_3=`perl -e 'use POSIX qw/strftime/; print strftime ("%Y %m %d\n", localtime( time - 259200 ));'| awk '{print $1"/"$2"/"$3}'`

check_halog() {
	TMP_RESULT=OK
	COMMAND="grep ERROR ${HALOG} | grep ${YEAR} "
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_res_today=$(echo "${_OUTPUT}" | grep "^${TODAY}" | wc -l)
	_res_minus_1=$(echo "${_OUTPUT}" | grep "^${TODAY_MINUS_1}" | wc -l)
	if [ ${_res_today} -ne 0 ]; then
	        LOG_FAIL_RESULT "Errors (${_res_today}) in ${HALOG} for ${TODAY}"
	        TMP_RESULT=NOK
	fi
	if [ ${_res_minus_1} -ne 0 ]; then
	        LOG_FAIL_RESULT "Errors (${_res_minus_1}) in ${HALOG} for ${TODAY_MINUS_1}"
	        TMP_RESULT=NOK
	fi
    if [ "${TMP_RESULT}" == "OK" ]; then
        LOG_OK_RESULT "No errors during last 4 days in halog"
    fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE="MS|NAS"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        check_halog
done
