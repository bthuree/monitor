#!/bin/bash

LoadAverage() {
	TMP_RESULT=OK
	COMMAND="uptime"
	MAX_AVG=2.5
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_avg=$(echo ${_OUTPUT} | /usr/xpg4/bin/awk -F'[a-z]:' '{ print $2 }'|awk '{print $3}')
	if [ `echo $_avg ${MAX_AVG} | awk '{ if ($1 > $2) {print 0}}'` ]; then
#		echo "15 minutes load average more than ${MAX_AVG}" 
		LOG_FAIL_RESULT "15 minutes Load Average to high"
		TMP_RESULT=NOK
	fi
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "Load Average ok"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

CHECK="Load Average"
TYPE="solaris|linux"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        LoadAverage
done
