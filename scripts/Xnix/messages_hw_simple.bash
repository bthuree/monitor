#!/bin/bash


hw_messages_simple() {
	TMP_RESULT=OK
	MONTH=`date +'%b'`
	DAY=`date +'%d'`
#	## Remove leading 0, '03' --> ' 3'
	DAY=$(echo "$DAY" | sed 's/^0/ /')

	TODAY_MINUS_1=`perl -e 'use POSIX qw/strftime/; print strftime ("%Y %b %d\n", localtime( time - 86400 ));'| awk '{print $2" "$3}'`
	M1=$(echo ${TODAY_MINUS_1} | awk '{print $1}')
	D1=$(echo ${TODAY_MINUS_1} | awk '{print $2}' | sed 's/^0/ /')

	LIST='WARN|CRIT|FAIL|NOTICE'
	LOGFILE='/var/adm/messages'

	if `LINUX`; then
	  LOGFILE="/var/log/messages"
	fi

	COMMAND="egrep '${MONTH} ${DAY}|${M1} ${D1}' $LOGFILE"
	OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	RESULT="`echo "$OUTPUT" | egrep "${LIST}" `"

	if [ -n "$RESULT" ]
	then
		TMP_RESULT=NOK
		LOG_FAIL_RESULT "Check ${LOGFILE} file. Possible HW Issues"
	else
		LOG_OK_RESULT "There are no obvious HW issues in ${LOGFILE} file"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

CHECK="HW Msg"
TYPE="solaris|linux"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        hw_messages_simple
done
