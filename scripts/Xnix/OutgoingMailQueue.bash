#!/bin/bash

check_if_mails(){
	if [ "$1" == "CLIENT" ]; then
		COMMAND="ls /var/spool/clientmqueue|grep -v sm-client.pid | wc -l"
	else
		COMMAND="ls /var/spool/mqueue| wc -l"
	fi
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_files="`echo "${_OUTPUT}" | awk '{print $1}'`"

	if [ ! -z ${_files} ]; then			# _files is defined
		if [ ${_files} -gt 0 ]; then	# If we have files...
		    return 0					# Return true to remove them
		fi
	fi
	return 1
}


mail_in_clientmqueue() {
	TMP_RESULT=OK
	if `check_if_mails CLIENT`; then 
		COMMAND="cd /var/spool; mv clientmqueue clientmqueue.old; mkdir clientmqueue; chown smmsp:smmsp clientmqueue; chmod 770 clientmqueue; rm -fr clientmqueue.old"
		_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
		LOG_FAIL_RESULT "Removed mail outqueue (/var/spool/clientmqueue not empty)"	
	fi
	if `check_if_mails CLIENT`; then 
		LOG_FAIL_RESULT "Mail in outqueue (/var/spool/clientmqueue not empty)"
		TMP_RESULT=NOK
	fi
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "Outgoing mail queue empty"
	fi
}

mail_in_mqueue() {
	TMP_RESULT=OK
	if `check_if_mails MQUEUE`; then 
		COMMAND="cd /var/spool; mv mqueue mqueue.old; mkdir mqueue; chown root:bin mqueue; chmod 750 mqueue; rm -fr mqueue.old"
		_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
		LOG_FAIL_RESULT "Removed mail outqueue (/var/spool/mqueue not empty)"	
	fi
	if `check_if_mails MQUEUE`; then 
		LOG_FAIL_RESULT "Mail in outqueue (/var/spool/mqueue not empty)"
		TMP_RESULT=NOK
	fi
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "Tmp Outgoing mail queue empty"
	fi

}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE="solaris|linux"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        mail_in_clientmqueue
		mail_in_mqueue        
done




# mail_in_clientmqueue() {
# 	TMP_RESULT=OK
# 	RemoveClientMqueue="cd /var/spool; mv clientmqueue clientmqueue.old; mkdir clientmqueue; chown smmsp:smmsp clientmqueue; chmod 770 clientmqueue; rm -fr clientmqueue.old"
# 	COMMAND="ls /var/spool/clientmqueue|grep -v sm-client.pid | wc -l"
# 	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
# 	_files=`echo "${_OUTPUT}" | awk '{print $1}'`
# 	if [ ${_files} -gt 0 ]; then
# 		COMMAND="${RemoveClientMqueue}"
# 		_OUTPUT="`SSH_SERVER_ERR_2_NULL`"

# 		LOG_FAIL_RESULT "Mail in outqueue (/var/spool/clientmqueue not empty)"
# 		TMP_RESULT=NOK
# 	fi
# 	if [ "${TMP_RESULT}" == "OK" ]; then
# 		LOG_OK_RESULT "Outgoing mail queue empty"
# 	fi
# }

# mail_in_mqueue() {
# 	TMP_RESULT=OK
# 	RemoveMqueue="cd /var/spool; mv mqueue mqueue.old; mkdir mqueue; chown root:bin mqueue; chmod 750 mqueue; rm -fr mqueue.old"
# 	COMMAND="ls /var/spool/mqueue| wc -l"
# 	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
# 	_files=`echo "${_OUTPUT}" | awk '{print $1}'`
# 	if [ ${_files} -gt 1 ]; then
# 		LOG_FAIL_RESULT "Mail in tmp outqueue (/var/spool/mqueue not empty)"
# 		TMP_RESULT=NOK
# 	fi
# 	if [ "${TMP_RESULT}" == "OK" ]; then
# 		LOG_OK_RESULT "Tmp Outgoing mail queue empty"
# 	fi
# }