#!/bin/bash

linux_local_hard_disk_issues(){
	COMMAND="df -Ph --exclude-type=vxfs | egrep -v '^Filesystem|tmpfs|devtmpfs|cdrom|lofi'"
	TMP_RESULT=OK
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	## Now we have a list of local sd disks, lets loop only the disks not the partitions
	echo $_OUTPUT | awk '{print $1}' | sed "s/[0-9]*$//" | sort | uniq |
	## To keep TMP_RESULT after while loop done
	{
		while read _disk;
		do
			if [ `echo $_disk | grep cciss` ]; then
				_disk="`echo $_disk | sed 's/p*$//'`"
				COMMAND="smartctl -H -d cciss,0 -a ${_disk}"
			else
				COMMAND="smartctl -H ${_disk}"
			fi
			TMP_RESULT=OK
			_OUTPUT2="`SSH_SERVER_ERR_2_NULL`"
			_res=`echo "$_OUTPUT2" | grep "SMART Health Status" | awk '{print $4}'`
			if [ "$_res" != "OK" ]; then
				LOG_FAIL_RESULT "Hard disk issues on ${_disk} smartctrl reports ${_res} "
				TMP_RESULT=NOK
			fi
		done
		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "Local Hard disks ok"
		fi
	}
}


solaris_local_hard_disk_issues(){
	## Only check MD devices, as well as cxtydz disks (that is local disks)
	COMMAND="iostat -en | egrep 'c[0-9]t[0-9]d[0-9]|md'"
	TMP_RESULT=OK
	SSH_SERVER_ERR_2_NULL |  
	## To keep TMP_RESULT after while loop done
	{
		while read output;
		do
			_disk=$(echo $output | awk  '{ print $5}'  )
			_issues=$(echo $output | awk  '{ print $1 " " $2 " " $3 " " $4 }' )
			if [ "${_issues}" != "0 0 0 0" ]; then
				LOG_FAIL_RESULT "Hard disk issues on ${_disk} error count ${_issues} "
				TMP_RESULT=NOK
			fi
		done
		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "Local Hard disks ok"
		fi
	}
}

local_hard_disk_issues() {
	if `LINUX`; then
		linux_local_hard_disk_issues
	else
		solaris_local_hard_disk_issues
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

CHECK="Hard disk issues"
TYPE="solaris|linux"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        local_hard_disk_issues
done

