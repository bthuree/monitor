#!/bin/bash

NTP_issues() {
	COMMAND="ntpq -p | egrep -v 'remote|=====|LOCAL|224.0.1.1'"
	TMP_RESULT=OK
	_AT_LEAST_ONE_SOURCE=NOK
	SSH_SERVER_ERR_2_NULL |  
	## To keep TMP_RESULT after while loop done
	{
		while read output;
		do
			_st=$(echo $output | awk  '{ print $3}'  )		## Stratum less than 5
			if [ $_st -ge 5 ] && [ "${TMP_RESULT}" == "OK" ]; then
				LOG_FAIL_RESULT "NTP TIME Issue (stratum > 5)"
				TMP_RESULT=NOK
			fi
			_reach=$(echo $output | awk  '{ print $7}'  ) 	## NTP Communication ok		
			if [ $_reach -ne 377 ] && [ "${TMP_RESULT}" == "OK" ]; then
				LOG_FAIL_RESULT "NTP TIME Issue (reach not 377)"
				TMP_RESULT=NOK
			fi
			_AT_LEAST_ONE_SOURCE=OK
		done
		if [ "${TMP_RESULT}" == "OK" ] && [ "${_AT_LEAST_ONE_SOURCE}" == "NOK" ]; then
			LOG_FAIL_RESULT "NTP TIME Issue (no sources)"
			TMP_RESULT=NOK
		fi
		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "NTP Time OK"
		fi
	}
}

NTP_issues_ntp4() {
	## NTP4 should not be used for the moment
	COMMAND="svcs ntp | tail -1"
	TMP_RESULT=OK
	_st="`SSH_SERVER_ERR_2_NULL | awk  '{ print $1}' `"
	if [ "${_st}" == "disabled" ]; then
		LOG_FAIL_RESULT "NTP3 is disabled"
		TMP_RESULT=NOK
	fi
	COMMAND="svcs ntp4 | tail -1"
	_st="`SSH_SERVER_ERR_2_NULL | awk  '{ print $1}' `"
	if [ "${_st}" == "online" ]; then
		LOG_FAIL_RESULT "NTP4 is online"
		TMP_RESULT=NOK
	fi
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "NTP Version check OK"
	fi
}


_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

CHECK="NTP"
TYPE="solaris|linux"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        NTP_issues
        if ! LINUX; then
        	NTP_issues_ntp4
        fi
done
