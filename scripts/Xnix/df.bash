#!/bin/bash



df_h () {
	if `LINUX`; then
		COMMAND="df -Ph"
	else
		COMMAND="df -lh"
	fi
	TMP_RESULT=OK
#	SSH_SERVER_ERR_2_NULL | egrep  -v '^Filesystem|tmpfs|cdrom|lofi|syblog|sybdata' | awk '{ print $5 " " $1 }' | 
	SSH_SERVER_ERR_2_NULL | egrep '^/dev' | egrep  -v 'tmpfs|lofi|syblog|sybdata' | awk '{ print $5 " " $1 }' | 
	## To keep TMP_RESULT after while loop done
	{
		while read output;
		do
			usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1  )
			partition=$(echo $output | awk '{ print $2 }' )
			if [ $usep -ge 98 ] && [ "${TMP_RESULT}" == "OK" ]; then
	#			echo "File system full  \"$partition ($usep%)\" on $SERVER as on $(date)" 
				LOG_FAIL_RESULT "File System FULL on ${partition}"
				TMP_RESULT=NOK
			fi
			if [ $usep -ge 90 ] && [ "${TMP_RESULT}" == "OK" ]; then
	#			echo "Running out of space \"$partition ($usep%)\" on $SERVER as on $(date)" 
				LOG_FAIL_RESULT "File System getting full on ${partition}"
				TMP_RESULT=NOK
			fi
		done
		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "File System space ok"
		fi
	}
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

CHECK="File system full"
TYPE="solaris|linux"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        df_h
done
