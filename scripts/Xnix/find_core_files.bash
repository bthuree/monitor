#!/bin/bash

TMPFILE=/tmp/TMP_FILE.$$.txt

find_core_files () {
	COMMAND="find / -name core -type f; "
	if `UAS`; then  # Only check local disk on UAS
		COMMAND="find / -name core -type f -xdev; "
		if [ ! $CHECK_OSS_MOUNTS ]; then ## Ignore finding any files on standby UAS. Mounting Issues
			COMMAND="find /DONTFINDANYFILES; "
		fi
	fi
	if `ENIQ`; then
		COMMAND="${COMMAND}; find /eniq/backup/core -name core_\*"
	fi
	if [ $CHECK_OSS_MOUNTS ]; then
		COMMAND="${COMMAND}; find /ossrc/upgrade/core -name core_\*;"
	fi

	TMP_RESULT=OK
	OUTPUT_ALL="`SSH_SERVER_ERR_2_NULL`"
	echo "${OUTPUT_ALL}" > ${TMPFILE}
	while read output;
	do
		# Lets confirm this is a core file
		COMMAND="file '$output'" 
		_file_output="`SSH_SERVER_ERR_2_NULL`"
		_res2=$(echo ${_file_output} | grep 'core file' | grep from)
		if [ "a${_res2}" != "a" ]; then
				LOG_FAIL_RESULT "Core File Found (${output})"
				TMP_RESULT=NOK
		fi
	done < ${TMPFILE}

	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "No core files where found"
	fi
	rm -f ${TMPFILE}
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash


CHECK_OSS_MOUNTS=TRUE

CHECK="Core files"
TYPE="solaris|linux"
SELECT_ENV

OSS_SRV=`GET_OSS_SERVER`

if `IS_GEO_CLUSTER`; then

        if `ACTIVE_SIDE_GEO_CLUSTER`; then
                CHECK_OSS_MOUNTS=TRUE
        else
                unset CHECK_OSS_MOUNTS
        fi

else
    CHECK_OSS_MOUNTS=TRUE
fi

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        find_core_files
done


