#!/bin/bash

Uptime_More_1_day() {
	TMP_RESULT=OK
	COMMAND="uptime"
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_days=`echo ${_OUTPUT} | /usr/xpg4/bin/awk -F'( |,|:)+' '{
	    if ($5=="min")
	        m=$4;
	    else {
	        if ($5~/^day/) { d=$4; h=$6; m=$7}
	        else {h=$4;m=$5}
	        }
	    }
	    {
	#        print d+0,"days,",h+0,"hours,",m+0,"minutes."
	        print d+0
    }'`
	if [ ${_days} == 0 ]; then
#		echo "Last reboot less than 24 hours ago" 
		LOG_FAIL_RESULT "Last reboot less than 24 hours ago"
		TMP_RESULT=NOK
	fi
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "Last reboot more than 24 hours ago"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

CHECK="Uptime"
TYPE="solaris|linux"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        Uptime_More_1_day
done
