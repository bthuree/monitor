#!/bin/bash

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

CHECK="Dead Servers"
TYPE="solaris|linux"
SELECT_ENV
SERVERS=`SELECT_SERVER`

SSH_TEST () {
    # Just check if a simple SSH connection works, by executing date -u via SSH, and searching for GMT in result string.
        COMMAND="date -u"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        if [ $? != 0 ]; then
        	return 1
        fi

        if [ "`echo "${_OUTPUT}" | egrep "GMT|UTC"`" ] ; then
                return 0
        fi
        return 1
}


TMP_RESULT=OK
for SERVER in ${SERVERS}
do
	PING
	if [ $? == 0 ]; then 
		LOG_OK_RESULT "Server are responding on PING (not dead)"
	else
		TMP_RESULT=NOK
		LOG_FAIL_RESULT "Server not reachable on PING (dead?)"
	fi
	SSH_TEST
	if [ $? == 0 ]; then 
		LOG_OK_RESULT "Server are responding on SSH (not dead)"
	else
		TMP_RESULT=NOK
		LOG_FAIL_RESULT "Server not reachable on SSH (dead?)"
	fi
done

