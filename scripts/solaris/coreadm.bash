#!/bin/bash

_GLOBAL_PATTERN_RULE_ADMIN="/ossrc/upgrade/core/core_%n_%f_%u_%g_%p"
_GLOBAL_PATTERN_RULE_ENIQ="/eniq/backup/core/core_%n_%f_%u_%g_%p"

_GLOBAL_PATTERN="global core file pattern"
_INIT_PATTERN="init core file pattern"
_GLOBAL_CORE="global core dumps"
_PERPROCESS_CORE="per-process core dumps"
_GLOBAL_SETID="global setid core dumps"
_PER_PROCESS_SETID="per-process setid core dumps"
_GLOBAL_LOGGING="global core dump logging"

# bash-3.2# coreadm
#      global core file pattern: /ossrc/upgrade/core/core_%n_%f_%u_%g_%p
#      global core file content: default
#        init core file pattern: /ossrc/upgrade/core/core_%n_%f_%u_%g_%p
#        init core file content: default
#             global core dumps: enabled
#        per-process core dumps: disabled
#       global setid core dumps: enabled
#  per-process setid core dumps: disabled
#      global core dump logging: enabled

CheckCoreAdmConfig () {
        if `ENIQ`; then
                _GLOBAL_PATTERN_RULE="${_GLOBAL_PATTERN_RULE_ENIQ}"
        else
                _GLOBAL_PATTERN_RULE="${_GLOBAL_PATTERN_RULE_ADMIN}"
        fi
        
        _GLOBAL_PATTERN_RULE_PRINT="`echo ${_GLOBAL_PATTERN_RULE} | sed 's/%/%%/g'`"

        TMP_RESULT=OK                
        COMMAND="coreadm"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _res_pattern=$(echo "$_OUTPUT" | grep "${_GLOBAL_PATTERN}" | awk -F: '{print $2}' | tr -d '[[:space:]]')
        _res_init_pattern=$(echo "$_OUTPUT" | grep "${_INIT_PATTERN}" | awk -F: '{print $2}' | tr -d '[[:space:]]')
        _res_global_core_enabled=$(echo "$_OUTPUT" | grep "${_GLOBAL_CORE}" | awk -F: '{print $2}' | tr -d '[[:space:]]')
        _res_perprocess_core_enabled=$(echo "$_OUTPUT" | grep "${_PERPROCESS_CORE}" | awk -F: '{print $2}' | tr -d '[[:space:]]')
        _res_global_setid=$(echo "$_OUTPUT" | grep "${_GLOBAL_SETID}" | awk -F: '{print $2}' | tr -d '[[:space:]]')
        _res_per_process_setid=$(echo "$_OUTPUT" | grep "${_PER_PROCESS_SETID}" | awk -F: '{print $2}' | tr -d '[[:space:]]')
        _global_logging=$(echo "$_OUTPUT" | grep "${_GLOBAL_LOGGING}" | awk -F: '{print $2}' | tr -d '[[:space:]]')

        if [ "${_res_pattern}" != "${_GLOBAL_PATTERN_RULE}" ]; then
                LOG_FAIL_RESULT "Wrong global core file pattern (expecting ${_GLOBAL_PATTERN_RULE_PRINT})"
                TMP_RESULT=NOK                
        fi
        if [ "${_res_init_pattern}" != "${_GLOBAL_PATTERN_RULE}" ]; then
                LOG_FAIL_RESULT "Wrong init core file pattern (expecting ${_GLOBAL_PATTERN_RULE_PRINT})"
                TMP_RESULT=NOK                
        fi
        if [ "${_res_global_core_enabled}" != "enabled" ]; then
                LOG_FAIL_RESULT "Not configured for global core dumps"
                TMP_RESULT=NOK                
        fi
        if [ "${_res_perprocess_core_enabled}" != "disabled" ]; then
                LOG_FAIL_RESULT "Configured for per-process core dumps"
                TMP_RESULT=NOK                
        fi
        if [ "${_res_global_setid}" != "enabled" ]; then
                LOG_FAIL_RESULT "Not configured for global setid core dumps"
                TMP_RESULT=NOK                
        fi
        if [ "${_res_per_process_setid}" != "disabled" ]; then
                LOG_FAIL_RESULT "Configured for per-process setid core dumps"
                TMP_RESULT=NOK                
        fi
        if [ "${_global_logging}" != "enabled" ]; then
                LOG_FAIL_RESULT "Not configured for global core dump logging"
                TMP_RESULT=NOK                
        fi

        if [ "${TMP_RESULT}" == "OK" ]; then
                LOG_OK_RESULT "coreadm configuration ok"
        fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

TYPE="MS|UAS|ENIQ"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
#        CheckCoreDirectory
        CheckCoreAdmConfig
done



