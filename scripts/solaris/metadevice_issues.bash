#!/bin/bash


metadevice_issues() {
	COMMAND="metadb"
	TMP_RESULT=OK
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	if [ `echo "${_OUTPUT}" | grep 'no existing databases' | wc -l` == 1 ]; then
		LOG_FAIL_RESULT "Disksuite: Not in use"
		TMP_RESULT=NOK
	elif [ `echo "${_OUTPUT}" | grep -v flags | grep -v a | wc -l` != 0 ]; then
		LOG_FAIL_RESULT "Disksuite: Inactive metadb(s) found"
		TMP_RESULT=NOK
	fi

	COMMAND="metastat"
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	if [ `echo "${_OUTPUT}" | grep 'no existing databases' | wc -l` != 1 ]; then

		if [ `echo "${_OUTPUT}" | grep State: | egrep -v Okay | wc -l` != 0  ]; then
			LOG_FAIL_RESULT "Disksuite: Metadevices are showing errors"
			TMP_RESULT=NOK
		fi
	fi

	COMMAND="echo | format"
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_nbr_disks=$(echo "${_OUTPUT}" | egrep -c '[0-9]\.')

	COMMAND="metastat -c"
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_virtual=$(echo "${_OUTPUT}" | awk  '{ print $2}' | grep -c m)
	_subdisk=$(echo "${_OUTPUT}" | awk  '{ print $2}' | grep -c s)

	if [ `expr ${_virtual} \* 2` -ne ${_subdisk} ] && [ ${_nbr_disks} -gt 1 ]; then
		LOG_FAIL_RESULT "Disksuite: One or more disks not mirrored"
		TMP_RESULT=NOK
	fi

	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "Disksuite: All metadevices are in Okay state"
	fi
}



_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE="solaris"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        metadevice_issues
done
