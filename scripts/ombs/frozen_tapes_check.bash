#!/bin/bash

# Check for FROZEN tapes.
## /usr/openv/netbackup/bin/goodies/available_media| grep FROZEN
##4710L4   HCART   TLD      0       42      -       1  775694474  FROZEN/MPX
##4719L4   HCART   TLD      0       28      -       1   24771584  FROZEN/MPX
##4704L4   HCART   TLD      0       47      -       1 1662402154     FROZEN

FrozenTapes() {
	TMP_RESULT=OK
	COMMAND='/usr/openv/netbackup/bin/goodies/available_media | grep -c "FROZEN"'
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_res=$(echo ${_OUTPUT})

	if [ ${_res} -gt 0 ]; then
		LOG_FAIL_RESULT "Frozen tapes detected"
		TMP_RESULT=NOK
	fi
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "No frozen tapes detected"
	fi

}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        FrozenTapes
done
