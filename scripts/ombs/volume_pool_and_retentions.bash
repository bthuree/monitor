#!/bin/bash


volume_pool_and_retentions() {
	TMP_RESULT=OK
	OLD_RES=""
	COMMAND='/usr/openv/netbackup/bin/admincmd/bppllist -U'
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	for POLICY in $(echo "${_OUTPUT}" | awk '{print $2}' | sort -u)
	do
		if [ ${POLICY} == "HotCatalog" ]; then
			continue
		fi
		COMMAND="/usr/openv/netbackup/bin/admincmd/bpplsched ${POLICY} -U"
		_OUTPUT2="`SSH_SERVER_ERR_2_NULL`"
		_res=$(echo "$_OUTPUT2" | grep Schedule:| awk '{print $2}')
		for SCHEDULE in ${_res}
		do
			COMMAND="/usr/openv/netbackup/bin/admincmd/bpplsched ${POLICY} -label ${SCHEDULE}"
			_OUTPUT3="`SSH_SERVER_ERR_2_NULL`"
			_mplex=$(echo "$_OUTPUT3" | grep "SCHED " | awk '{print $4}')
			_vpool=$(echo "$_OUTPUT3" | grep "SCHEDPOOL" | awk '{print $2}')
			_retention=$(echo "$_OUTPUT3" | grep "SCHEDRL" | awk '{print $2}')

			if [ ${_mplex} -ne 4 ]; then
				LOG_FAIL_RESULT "Multiplex wrong on ${POLICY} ${SCHEDULE}"
				TMP_RESULT=NOK
			fi

			if [ "${SCHEDULE}" == "Full" -o "${SCHEDULE}" == "ByMonthlyFull" -o "${SCHEDULE}" == "ByMonthly_Full" ]; then
				if [ "$_vpool" != "12-Weeks" -a "$_vpool" != "12weeks" ] || [ ${_retention} -ne 5 ]; then
					LOG_FAIL_RESULT "Pool or Rentention wrong on ${POLICY} ${SCHEDULE}"
					TMP_RESULT=NOK
				fi
			elif [ "${SCHEDULE}" == "Weekly_Full"  ]; then
				if [ "$_vpool" != "04-Weeks" -a "$_vpool" != "04weeks" ] || [ ${_retention} -ne 3 ]; then
					LOG_FAIL_RESULT "Pool or Rentention wrong on ${POLICY} ${SCHEDULE}"
					TMP_RESULT=NOK
				fi
			elif [ "${SCHEDULE}" == "Daily_Incr" -o "${SCHEDULE}" == "Daily_incr" ]; then
				if [ "$_vpool" != "02-Weeks" -a "$_vpool" != "02weeks" ] || [ ${_retention} -ne 1 ]; then
					LOG_FAIL_RESULT "Pool or Rentention wrong on ${POLICY} ${SCHEDULE}"
					TMP_RESULT=NOK
				fi
			elif [ "${SCHEDULE}" == "Weekly_Cum" -o "${SCHEDULE}" == "WeeklyCummulative"  ]; then
				if [ "$_vpool" != "04-Weeks" -a "$_vpool" != "04weeks" ] || [ ${_retention} -ne 3 ]; then
					LOG_FAIL_RESULT "Pool or Rentention wrong on ${POLICY} ${SCHEDULE}"
					TMP_RESULT=NOK
				fi
			elif [ "${SCHEDULE}" == "LTS"  ]; then
				if [ "$_vpool" != "52-Weeks" -a "$_vpool" != "52weeks" ] || [ ${_retention} -ne 8 ]; then
					LOG_FAIL_RESULT "Pool or Rentention wrong on ${POLICY} ${SCHEDULE}"
					TMP_RESULT=NOK
				fi
			elif [ "${SCHEDULE}" == "Full_Backup"  ]; then
				if [ "$_vpool" != "04-Weeks" -a "$_vpool" != "04weeks" ] || [ ${_retention} -ne 3 ]; then
					LOG_FAIL_RESULT "Pool or Rentention wrong on ${POLICY} ${SCHEDULE}"
					TMP_RESULT=NOK
				fi
			else
				LOG_FAIL_RESULT "Unknown Backup Schedule (${SCHEDULE} on ${POLICY})"
				TMP_RESULT=NOK
			fi
		done
	done
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "Volume pool, retention time and Multiplex OK"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        volume_pool_and_retentions
done

