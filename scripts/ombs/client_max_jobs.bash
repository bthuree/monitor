#!/bin/bash


client_max_jobs() {
        TMP_RESULT=OK
        COMMAND='/usr/openv/netbackup/bin/admincmd/bpclient -All'
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        for CLIENT in $(echo "${_OUTPUT}" | grep Name:|awk '{print $3}'|sort -u)
        do
                COMMAND="/usr/openv/netbackup/bin/admincmd/bpclient -client $CLIENT -l"
                _OUTPUT2="`SSH_SERVER_ERR_2_NULL`"
                _res=$(echo "$_OUTPUT2" | tail -2|awk '{print $5}')
                if [ ${_res} -ne 0 ]; then
                        LOG_FAIL_RESULT "Client ${CLIENT} Properties has limited Max Jobs"
                        TMP_RESULT=NOK
                fi
        done
        if [ "${TMP_RESULT}" == "OK" ]; then
                LOG_OK_RESULT "All clients unlimited max jobs"
        fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        client_max_jobs
done

