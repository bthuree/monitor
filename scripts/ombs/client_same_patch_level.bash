#!/bin/bash


client_same_patch_level() {
	TMP_RESULT=OK
	OLD_RES=""
	COMMAND='/usr/openv/netbackup/bin/admincmd/bppllist -l -allpolicies '
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	for CLIENT in $(echo "${_OUTPUT}" | grep CLIENT | awk '{print $2}' | sort -u)
	do
		COMMAND="/usr/openv/netbackup/bin/admincmd/bpgetconfig -s $CLIENT -A -L"
		_OUTPUT2="`SSH_SERVER_ERR_2_NULL`"
		_res=$(echo "$_OUTPUT2" | grep 'Patch Level' | awk '{print $4}')
		if [ "${OLD_RES}" != "${_res}" ] && [ "a${OLD_RES}" != "a" ]; then
			LOG_FAIL_RESULT "Different Client Patch Levels ($OLD_RES and $_res)"
			TMP_RESULT=NOK
		fi
		OLD_RES=${_res}
	done
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "All clients has same patch level"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        client_same_patch_level
done
