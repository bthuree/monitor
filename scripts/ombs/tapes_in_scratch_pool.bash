#!/bin/bash


tapes_in_scratch_pool() {
	TMP_RESULT=OK
	COMMAND='/usr/openv/volmgr/bin/vmquery -pn ScratchPool | grep -c "robot slot"'
	_OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	_res=$(echo ${_OUTPUT})

	if [ ${_res} -lt 1 ]; then
		LOG_FAIL_RESULT "No tapes in ScratchPool"
		TMP_RESULT=NOK
	fi
	if [ ${_res} -lt 5 ] && [ "${TMP_RESULT}" == "OK" ]; then
		LOG_FAIL_RESULT "Four or less tapes in ScratchPool"
		TMP_RESULT=NOK
	fi
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "Tapes ($_res) available in ScratchPool"
	fi

}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        tapes_in_scratch_pool
done
