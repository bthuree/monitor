#!/bin/bash


hotcalog_emergency_backups () {
        TMP_RESULT=OK
        COMMAND='find /export/ombs/catalog -type f -ctime -1'
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _res=$(echo "$_OUTPUT" | wc -l)
        if [ ${_res} -lt 4 ]; then
                LOG_FAIL_RESULT "Not enough HotCatalog Emergency Backups (${_res} stored during last 24 hours)"
                TMP_RESULT=NOK
        fi
        if [ "${TMP_RESULT}" == "OK" ]; then
                LOG_OK_RESULT "HotCatalog Emergency Backups OK"
        fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        hotcalog_emergency_backups
done


