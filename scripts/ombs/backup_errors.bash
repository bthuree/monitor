#!/bin/bash


check_backup_errors() {
        TMP_RESULT=OK
        for _hours in 2 4 6 8 12 24 48 72
        do

                COMMAND="/usr/openv/netbackup/bin/admincmd/bperror -l  -backstat -s info -hoursago ${_hours}"
                _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
                _res=$(echo "${_OUTPUT}" | /usr/xpg4/bin/awk -F'EXIT STATUS' '{print $2}' | awk '{print $1}'|grep -v '^$'|grep -v 0| wc -l)

                if [ ${_res} -eq 0 ]
                then
                        continue
                else
                        LOG_FAIL_RESULT "Failed backups (${_res}) during last ${_hours} hours"
                        TMP_RESULT=NOK
                        break
                fi
        done
        if [ ${_res} -eq 0 ]
        then
                LOG_OK_RESULT "All backups successfull during last ${_hours} hours"
        fi

}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        check_backup_errors
done
