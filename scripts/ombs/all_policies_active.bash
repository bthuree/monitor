#!/bin/bash


all_policies_active() {
	TMP_RESULT=OK
	COMMAND='/usr/openv/netbackup/bin/admincmd/bppllist -U'
	_OUTPUT_LIST="`SSH_SERVER_ERR_2_NULL`"
	for P in $(echo "${_OUTPUT_LIST}" | awk '{print $2}')
	do
		_CHECK_IF_ACTIVE=YES
		## If LDAP_CLIENT List = 0, and P <> uas|ENIQ|OSS
		if [ ${_LDAP_CLIENTS} -eq 0 ]; then	
			if [[ ${P} == ENIQ* ]] || [[ ${P} == OSS* ]] || [[ ${P} == emuas* ]]; then
				_CHECK_IF_ACTIVE=NO
			fi
		fi

		if [ "${_CHECK_IF_ACTIVE}" == "YES" ]; then
			COMMAND="/usr/openv/netbackup/bin/admincmd/bpplinfo ${P} -U"
			_OUTPUT_POLICY="`SSH_SERVER_ERR_2_NULL`"
			_act=$(echo "${_OUTPUT_POLICY}" | grep "Active:" | awk '{print $2}')
			if [ "${_act}" != "yes" ]; then
				LOG_FAIL_RESULT "Policy ($P) disabled"
				TMP_RESULT=NOK
			fi
		fi
	done
	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "All policies active"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

_LDAP_CLIENTS=2

if `IS_GEO_CLUSTER`; then
        if `ACTIVE_SIDE_GEO_CLUSTER`; then
                _LDAP_CLIENTS=2
        else
                _LDAP_CLIENTS=0
        fi
else
		_LDAP_CLIENTS=2
fi

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        all_policies_active
done
