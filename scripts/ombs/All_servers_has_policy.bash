#!/bin/bash

TMP_RESULT=OK

Check_Policy_Exist() {
	SRV=$1
	PolicyNameBeginning=$2
	NmbrOfPoliciesNeeded=$3
	Policies="$4"

	FoundNbrPolicies=`echo "${Policies}" | awk '{print $2}' | grep -c ${PolicyNameBeginning}`

	if [ ${FoundNbrPolicies} -ne ${NmbrOfPoliciesNeeded} ]; then
		LOG_FAIL_RESULT "Policy for ${SRV} not found (expected ${NmbrOfPoliciesNeeded} found ${FoundNbrPolicies})"
		TMP_RESULT=NOK
	fi

}



all_servers_has_a_policy() {

	COMMAND='/usr/openv/netbackup/bin/admincmd/bppllist -U'
	_OUTPUT_LIST="`SSH_SERVER_ERR_2_NULL`"

	Check_Policy_Exist ENIQ		ENIQ_STATS	3	"${_OUTPUT_LIST}"
	Check_Policy_Exist Catalog 	Catalog		1	"${_OUTPUT_LIST}"
	Check_Policy_Exist OSS-RC	OSS_i386	4	"${_OUTPUT_LIST}"
	Check_Policy_Exist BIS 		embis		1	"${_OUTPUT_LIST}"
	Check_Policy_Exist MWS 		emmws		1	"${_OUTPUT_LIST}"
	Check_Policy_Exist NEDSS 	emned		1	"${_OUTPUT_LIST}"
	Check_Policy_Exist COMInf 	emoam		2	"${_OUTPUT_LIST}"
	Check_Policy_Exist OMSAS 	emoas		1	"${_OUTPUT_LIST}"
	Check_Policy_Exist OMBS 	emomb		1	"${_OUTPUT_LIST}"
	Check_Policy_Exist UAS 		emuas		2	"${_OUTPUT_LIST}"
	Check_Policy_Exist WAS 		emwas		1	"${_OUTPUT_LIST}"
	Check_Policy_Exist BSB 		stbsb		1	"${_OUTPUT_LIST}"

	if [ "${TMP_RESULT}" == "OK" ]; then
		LOG_OK_RESULT "All servers has a policy"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=OMBS
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        all_servers_has_a_policy
done


