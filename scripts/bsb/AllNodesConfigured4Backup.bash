#!/bin/bash

NmrNodes2Backup=10

AllNodesConfigured4Backup () {
        TMP_RESULT=OK
        COMMAND="grep emsback /etc/passwd"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _HOMEDIR=$(echo "${_OUTPUT}" | awk -F: '{print $6}')

        COMMAND="grep _HOST ${_HOMEDIR}/emsbackup.ini"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        __NbrNodes=$(echo "${_OUTPUT}" | grep -v "^#" | awk -F= '{print $2}' | tr "'" '"' | awk -F\" '{print $2}'| tr '\n' ' '|wc -w)

        # Check number of nodes configured for backup
        if [ "${__NbrNodes}" -eq "${NmrNodes2Backup}" ]; then
			LOG_OK_RESULT "${NmrNodes2Backup} nodes configured for backup"
        else
			LOG_FAIL_RESULT "Not ${NmrNodes2Backup} nodes configured for backup"
			TMP_RESULT=NOK
        fi      
}



_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=BSB
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        AllNodesConfigured4Backup
done
