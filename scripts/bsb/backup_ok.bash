#!/bin/bash

OK1="SOEM Backup Successful"
OK2="NetOp Backup Successful"
OK3="RDM Backup Successful"
OK4="EMA Backup Successful"
ALLOK="BSB Backup was Successful"

ems_backup_ok () {
        TMP_RESULT=OK
        COMMAND="ls -lart /backup/emsbackup*.log"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _BACKUP_RESULT_FILE=$(echo "${_OUTPUT}" | tail -1 | awk '{print $9}')
        _BACKUP_RESULT_DAY=$(echo "${_OUTPUT}" | tail -1 | awk '{print $7}')
        _TODAY_DAY=`date +"%d"|sed "s/^0//"`
        _YESTERDAY_DAY=`perl -e 'print scalar localtime( time - 86400 ) . "\n";' | awk '{print $3}'`

        # Check if first 9 characters in filename is emsbackup
        if [ "${_BACKUP_RESULT_DAY}" == "${_TODAY_DAY}" ] || [ "${_BACKUP_RESULT_DAY}" == "${_YESTERDAY_DAY}" ]; then
                COMMAND="grep -i successful ${_BACKUP_RESULT_FILE}"
                _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
                _res=$(echo "$_OUTPUT" | egrep -i "${OK1}|${OK2}|${OK3}|${OK4}|${ALLOK}" | wc -l )
                if [ ${_res} -ne 5 ]; then
                        LOG_FAIL_RESULT "One or more of RDM, SOEM, EMA or Netop failed"
                        TMP_RESULT=NOK
                else
                        LOG_OK_RESULT "Last BSB EMS backup successfull"
                fi
        else
                LOG_FAIL_RESULT "Last BSB EMS backup is to old"
                TMP_RESULT=NOK
        fi      

}

ne_backup_ok () {
        TMP_RESULT=OK
        _BACKUP_RESULT_FILE="/backup/nebackup.summary.log"
        COMMAND="head -1 ${_BACKUP_RESULT_FILE}"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _BACKUP_RESULT_DAY=$(echo "${_OUTPUT}" | awk '{print $3}')
        _TODAY_DAY=`date +"%d"|sed "s/^0//"`

        if [ ${_BACKUP_RESULT_DAY} == ${_TODAY_DAY} ]; then
                DATE_OK="OK"
        else
                DATE_OK="NOK"
        fi

        # If backup was done today.
        if [ ${DATE_OK} == "OK" ]; then
                COMMAND="grep NOK ${_BACKUP_RESULT_FILE}"
                _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
                # PGNGN      proclog can be NOK, since it is OK only when things has changed hence backup available. 
                _res=$(echo "$_OUTPUT" | grep -v "PGNGN      proclog" | tr -d '[:space:]' | wc -c )
                if [ ${_res} -gt 0 ]; then
                        LOG_FAIL_RESULT "One or more of PGNGN, DNS, CUDB, HSS or SAPC failed"
                        TMP_RESULT=NOK
                else
                        LOG_OK_RESULT "Last BSB NE backup successfull"
                fi
        else
                LOG_FAIL_RESULT "Last BSB NE backup is to old"
                TMP_RESULT=NOK
        fi      

}



_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=BSB
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        ems_backup_ok
        ne_backup_ok
done


