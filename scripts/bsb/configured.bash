#!/bin/bash


configured () {
        TMP_RESULT=OK
        COMMAND='file /backup'
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _res=$(echo "$_OUTPUT" | awk '{print $2}')
        if [ "a${_res}" == "a" ]; then
                LOG_FAIL_RESULT "/backup directory do not exist"
                TMP_RESULT=NOK
        fi
        COMMAND='grep emsback /etc/passwd'
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _res=$(echo "$_OUTPUT" | awk -F: '{print $6}')
        if [ "a${_res}" == "a" ]; then
                LOG_FAIL_RESULT "emsback user do not exist"
                TMP_RESULT=NOK
        fi
        COMMAND='crontab -l emsback'
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
        _res=$(echo "$_OUTPUT" | grep -v "^#")
        if [ "a${_res}" == "a" ]; then
                LOG_FAIL_RESULT "emsback cronjob do not exist"
                TMP_RESULT=NOK
        fi

        if [ "${TMP_RESULT}" == "OK" ]; then
                LOG_OK_RESULT "BSB configured"
        fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=BSB
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        configured
done


