#!/bin/bash

#/ossrc/sybdev/sybmaster/var/opt/sybase/sybase/log
#egrep -i "Error:|infected|WARNING:|severity|encountered" masterdataservice.ERRORLOG

#cat /ossrc/sybdev/sybmaster/var/opt/sybase/sybase/log/masterdataservice.ERRORLOG| egrep -v "XP|JS" | nawk '/(kernel|server).*Error.*Severity.*State/ {printf "%s ", substr($0,47); e; print substr($0,27)}' "/ossrc/sybdev/sybmaster/var/opt/sybase/sybase/log/masterdataservice.ERRORLOG"

YEAR=`date +'%Y'`
MONTH=`date +'%m'`
DAY=`date +'%d'`

TODAY=${YEAR}/${MONTH}/${DAY}
TODAY_MINUS_1=`perl -e 'use POSIX qw/strftime/; print strftime ("%Y %m %d\n", localtime( time - 86400 ));'| awk '{print $1"/"$2"/"$3}'`
TODAY_MINUS_2=`perl -e 'use POSIX qw/strftime/; print strftime ("%Y %m %d\n", localtime( time - 172800 ));'| awk '{print $1"/"$2"/"$3}'`
TODAY_MINUS_3=`perl -e 'use POSIX qw/strftime/; print strftime ("%Y %m %d\n", localtime( time - 259200 ));'| awk '{print $1"/"$2"/"$3}'`


sybase_error_log() {
	TMP_RESULT=OK
	LIST='error:|state:|severity:|infected|warning:|encountered'
	LOGFILE='/ossrc/sybdev/sybmaster/var/opt/sybase/sybase/log/masterdataservice.ERRORLOG'

	COMMAND="egrep '${TODAY}|${TODAY_MINUS_1}' $LOGFILE"
	OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	RESULT="`echo "$OUTPUT" | egrep -i "${LIST}" | awk  '{print $1}' `"

	if [ -n "$RESULT" ]
	then
		TMP_RESULT=NOK
		LOG_FAIL_RESULT "Check ${LOGFILE} file. Possible HW Issues during last two days"
	else
		LOG_OK_RESULT "There are no obvious HW issues in ${LOGFILE} file"
	fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

SELECT_ENV

SYBASE_SRV=`GET_SYBASE_SERVER`
SERVER=`GET_IP_FOR_NAME ${SYBASE_SRV}`

if `IS_GEO_CLUSTER`; then

        if `ACTIVE_SIDE_GEO_CLUSTER`; then
                sybase_error_log
        else
                LOG_OK_RESULT "Passive cluster side, not checking"
        fi

else
        sybase_error_log
fi
