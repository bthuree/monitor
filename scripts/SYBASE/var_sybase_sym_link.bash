#!/bin/bash

# Ensure that /var/opt/sybase/sybase is a link to /ossrc/sybdev/sybmaster/var/opt/sybase/sybase
# lrwxrwxrwx   1 root     root          45 Nov  2  2016 /var/opt/sybase/sybase -> /ossrc/sybdev/sybmaster/var/opt/sybase/sybase

sybase_link() {
	TMP_RESULT=OK

	COMMAND="ls -lad /var/opt/sybase/sybase "
    _OUTPUT="`SSH_SERVER_ERR_2_NULL`"
	OUTPUT="`echo "${_OUTPUT}" | grep -v 'ossrc/sybdev/sybmaster'`"


    if [ -n "${OUTPUT}" ]; then
		LOG_FAIL_RESULT "/var/opt/sybase/sybase is NOT a sym link"
        TMP_RESULT=NOK
    else
		LOG_OK_RESULT "/var/opt/sybase/sybase is a sym link"
    fi

}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

TYPE="MS"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        sybase_link
done

