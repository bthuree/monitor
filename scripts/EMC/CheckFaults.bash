#!/bin/bash

check_for_faults() {
	OK_TXT="The array is operating normally"

	OUTPUT="`naviseccli -h ${SERVER} faults -list 2> /dev/null`"
	_res2=$(echo "${OUTPUT}" | grep "${OK_TXT}")

	if [ "a${_res2}" != "a" ]; then
				LOG_OK_RESULT "No faults on EMC ${SERVER}"
	else
				LOG_FAIL_RESULT "Faults found on EMC ${SERVER} : ${OUTPUT}"
	fi
}

check_hotspares_in_use() {
	# SIT1  6, 	# SIT2S 7 , 	# SIT2M 7, 	# PSMS  6, 	# PSMM  7

	# Type:                    4093: Hot Spare 
	# State:                   Hot Spare Ready
	# Hot Spare:               4093: YES 
	# Hot Spare Replacing:     Inactive

	# Type:                    4092: Hot Spare 
	# State:                   Enabled
	# Hot Spare:               4092: YES 
	# Hot Spare Replacing:     0_1_9
	OK_TXT="0|1"

	OUTPUT="`naviseccli -h ${SERVER} getdisk 2> /dev/null | grep 'Hot Spare Replacing' | grep -c -v 'Inactive'`" 
	_res2=$(echo "${OUTPUT}" | egrep "${OK_TXT}")

	if [ "a${_res2}" != "a" ]; then
				LOG_OK_RESULT "0 or 1 Hotspare is in use on EMC ${SERVER}"
	else
				LOG_FAIL_RESULT "To many hotspares is in use on EMC ${SERVER} : ${OUTPUT}"
	fi
}

check_all_disks_Enabled() {

	OUTPUT="`naviseccli -h ${SERVER} getdisk 2> /dev/null | grep -i state | egrep -v 'Hot Spare Ready|Enabled|Unbound'`"
	_res2=$(echo "${OUTPUT}")

	if [ "a${_res2}" == "a" ]; then
				LOG_OK_RESULT "All disks enabled on EMC ${SERVER}"
	else
				LOG_FAIL_RESULT "Some disks not in normal (Enabled) state on EMC ${SERVER} : ${OUTPUT}"
	fi
}


_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

#CHECK="EMC Faults"
TYPE="EMC"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        check_for_faults
        check_hotspares_in_use
        check_all_disks_Enabled
done
