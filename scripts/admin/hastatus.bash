#!/bin/bash

ALLOWED_OFFLINE="EBA_EBSS|EBA_EBSW"
TSS_PROC="TSSAuthorityMC"
GEO_OSS_PROCESS="ClusterService"

hastatus () {
        TMP_RESULT=OK                

        SERVER="${OSS_SRV}"
        COMMAND="/opt/VRTSvcs/bin/hastatus -sum"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"

        _SRVS_OK=$(echo "$_OUTPUT" | grep "^A" | grep RUNNING | wc -l)

        _REMOTE_SRVS_OK=$(echo "$_OUTPUT" | grep "^O" | grep RUNNING | wc -l)

        _SYB_OK=$(echo "$_OUTPUT" | grep "^B" | grep ONLINE | awk '{print $2}' | grep "Sybase1$")
        _OSS_OK=$(echo "$_OUTPUT" | grep "^B" | grep ONLINE | awk '{print $2}' | grep "Oss$")

        _REMOTE_SYB_OK=$(echo "$_OUTPUT" | grep "^P" | grep ONLINE | awk '{print $2}' | grep "Sybase1$")
        _REMOTE_OSS_OK=$(echo "$_OUTPUT" | grep "^P" | grep ONLINE | awk '{print $2}' | grep "Oss$")

        _GEO_DEF=$(echo "$_OUTPUT" | grep "^B" | grep $GEO_OSS_PROCESS)


        if [ ${_SRVS_OK} -ne 2 ]; then
                LOG_FAIL_RESULT "At least one server is not running"
                TMP_RESULT=NOK                
        fi

        if [ "x${_GEO_DEF}" == "x" ]; then
                ## Single cluster, not geo redundancy
                if [ "x${_SYB_OK}" == "x" ] || [ "x${_OSS_OK}" == "x" ]; then 
                        LOG_FAIL_RESULT "OSS or Sybase is not online"
                        TMP_RESULT=NOK                
                fi
        else
                ## Geo redundancy, two sets of clusters
                if [ ${_REMOTE_SRVS_OK} -ne 2 ]; then
                        LOG_FAIL_RESULT "At least one remote server is not running"
                        TMP_RESULT=NOK                
                fi

                if [ "x${_SYB_OK}" == "x" ] && [ "x${_REMOTE_SYB_OK}" == "x" ]; then 
                        LOG_FAIL_RESULT "Sybase is not online"
                        TMP_RESULT=NOK                
                fi                   
                if [ "x${_OSS_OK}" == "x" ] && [ "x${_REMOTE_OSS_OK}" == "x" ]; then 
                        LOG_FAIL_RESULT "OSS is not online"
                        TMP_RESULT=NOK                
                fi                   

        fi

        if [ "${TMP_RESULT}" == "OK" ]; then
                LOG_OK_RESULT "HA status looks ok"
        fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

TYPE=MS
SELECT_ENV

OSS_SRV=`GET_OSS_SERVER`

if `IS_GEO_CLUSTER`; then

        if `ACTIVE_SIDE_GEO_CLUSTER`; then
                hastatus
        else
                LOG_OK_RESULT "Passive cluster side, not checking"
        fi

else
        hastatus
fi
