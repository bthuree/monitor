#!/bin/bash

ALLOWED_OFFLINE="EBA_EBSS|EBA_EBSW"
TSS_PROC="TSSAuthorityMC"

right_servers () {
        OSS_SRV=`GET_OSS_SERVER`
        SYBASE_SRV=`GET_SYBASE_SERVER`

        TMP_RESULT=OK
        TYPE=MS1
        SELECT_ENV
        SERVER=`SELECT_SERVER`

        if [ "${OSS_SRV}" != "`SERVER_NAME`" ]; then
                LOG_FAIL_RESULT "OSS on wrong server"
                TMP_RESULT=NOK
        fi
 
        TYPE=MS2
        SELECT_ENV
        SERVER=`SELECT_SERVER`

        if [ "${SYBASE_SRV}" != "`SERVER_NAME`" ]; then
                LOG_FAIL_RESULT "Sybase on wrong server"
                TMP_RESULT=NOK
        fi
               
        if [ "${TMP_RESULT}" == "OK" ]; then
                LOG_OK_RESULT "OSS and Sybase on correct servers"
        fi
}


_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash
. ${_MYPATH}/../../lib/common_admin_srvs.bash

if `IS_GEO_CLUSTER`; then

        if `ACTIVE_SIDE_GEO_CLUSTER`; then
                right_servers
        else
                LOG_OK_RESULT "Passive cluster side, not checking"
        fi

else
        right_servers
fi
