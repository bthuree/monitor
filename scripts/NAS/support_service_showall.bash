#!/bin/bash

# emnas00003e2> support services showall 

#                   emnas00003e2
# Service           01       02    
# -------         -------- -------- 
# nfs               ONLINE   ONLINE 
# cifs             OFFLINE  OFFLINE 
# ftp              OFFLINE  OFFLINE 
# http             OFFLINE  OFFLINE 
# iSCSIInitiator   OFFLINE  OFFLINE 
# console           ONLINE  OFFLINE 
# gui              OFFLINE  OFFLINE 
# antivirus        OFFLINE  OFFLINE 
# nic_pubeth0       ONLINE   ONLINE 
# nic_pubeth1       ONLINE   ONLINE 
# nic_pubeth2       ONLINE   ONLINE 
# nic_pubeth3       ONLINE   ONLINE 
# nic_pubeth4       ONLINE   ONLINE 
# nic_pubeth5       ONLINE   ONLINE 
# fs_manager        ONLINE   ONLINE 
# dedup_scheduler  OFFLINE  OFFLINE 
# 192.168.23.211   OFFLINE   ONLINE 
# 192.168.23.220   OFFLINE   ONLINE 
# 192.168.23.221   OFFLINE   ONLINE 
# 192.168.23.222    ONLINE  OFFLINE 
# 192.168.23.212    ONLINE  OFFLINE 
# 192.168.23.213   OFFLINE   ONLINE 
# 192.168.23.214    ONLINE  OFFLINE 
# 192.168.23.215   OFFLINE   ONLINE 
# 192.168.23.216    ONLINE  OFFLINE 
# 192.168.23.217   OFFLINE   ONLINE 
# 192.168.23.218    ONLINE  OFFLINE 
# 192.168.23.219    ONLINE  OFFLINE 
# /vx/oss1a-nms_cosm   ONLINE   ONLINE 
# /vx/oss1a-home    ONLINE   ONLINE 
# /vx/oss1a-ddc_data   ONLINE   ONLINE 
# /vx/smrs1-core_common   ONLINE   ONLINE 
# /vx/smrs1-core_emned01e2   ONLINE   ONLINE 
# /vx/smrs1-gran_common   ONLINE   ONLINE 
# /vx/smrs1-gran_emned01e2   ONLINE   ONLINE 
# /vx/smrs1-lran_common   ONLINE   ONLINE 
# /vx/smrs1-lran_emned01e2   ONLINE   ONLINE 
# /vx/smrs1-wran_common   ONLINE   ONLINE 
# /vx/oss1a-segment1   ONLINE   ONLINE 
# /vx/smrs1-wran_emned01e2   ONLINE   ONLINE 
# /vx/oss1a-eniq_pm_vol   ONLINE   ONLINE 
# /vx/eniqsp1-admin   ONLINE   ONLINE 
# /vx/eniqsp1-archive   ONLINE   ONLINE 
# /vx/eniqsp1-backup   ONLINE   ONLINE 
# /vx/eniqsp1-etldata   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-00   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-01   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-02   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-03   ONLINE   ONLINE 
# /vx/oss1a-sgwcg   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-04   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-05   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-06   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-07   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-08   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-09   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-10   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-11   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-12   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-13   ONLINE   ONLINE 
# /vx/oss1a-eba_rede   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-14   ONLINE   ONLINE 
# /vx/eniqsp1-etldata_-15   ONLINE   ONLINE 
# /vx/eniqsp1-fmdata   ONLINE   ONLINE 
# /vx/eniqsp1-home   ONLINE   ONLINE 
# /vx/eniqsp1-log   ONLINE   ONLINE 
# /vx/eniqsp1-pmdata   ONLINE   ONLINE 
# /vx/eniqsp1-reference   ONLINE   ONLINE 
# /vx/eniqsp1-rejected   ONLINE   ONLINE 
# /vx/eniqsp1-sentinel   ONLINE   ONLINE 
# /vx/eniqsp1-snapshot   ONLINE   ONLINE 
# /vx/oss1a-eba_ebsg   ONLINE   ONLINE 
# /vx/eniqsp1-sql_anywhere   ONLINE   ONLINE 
# /vx/eniqsp1-sybase_iq   ONLINE   ONLINE 
# /vx/eniqsp1-sw    ONLINE   ONLINE 
# /vx/eniqsp1-upgrade   ONLINE   ONLINE 
# /vx/eniqsp1-pmdata_wifi   ONLINE   ONLINE 
# /vx/eniqsp1-pmdata_soem   ONLINE   ONLINE 
# /vx/eniqsp1-pmdata_sim   ONLINE   ONLINE 
# /vx/oss1a-eba_ebsw   ONLINE   ONLINE 
# /vx/oss1a-eba_ebss   ONLINE   ONLINE 
# /vx/oss1a-eba_rtt   ONLINE   ONLINE 
# /vx/oss1a-pm_storage   ONLINE   ONLINE 
# replication      RUNNING  STOPPED 

public_ethernet() {
	TMP_RESULT=OK

	## Send CHK_CMD command
	COMMAND="support services showall"
	SSH_SERVER_ERR_2_NULL master | grep 'pubeth' |
	{
		while read output;
		do
			eth=$(echo $output | awk '{ print $1}' )
			ok1=$(echo $output | awk '{ print $2}' )
			ok2=$(echo $output | awk '{ print $3}' )
			if [ "${ok1}" != "ONLINE" ] || [ "${ok2}" != "ONLINE" ]; then
				LOG_FAIL_RESULT "NAS Public Eth ${Eth} is faulty"
				TMP_RESULT=NOK
			fi
		done

		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "Public Ethernet is OK"
			TMP_RESULT=OK
		fi
	}
}

vx_mounts() {
	TMP_RESULT=OK

	## Send CHK_CMD command
	COMMAND="support services showall"
	SSH_SERVER_ERR_2_NULL master | grep '\/vx\/' |
	{
		while read output;
		do
			vx=$(echo $output | awk '{ print $1}' )
			ok1=$(echo $output | awk '{ print $2}' )
			ok2=$(echo $output | awk '{ print $3}' )
			if [ "${ok1}" != "ONLINE" ] || [ "${ok2}" != "ONLINE" ]; then
				LOG_FAIL_RESULT "NAS VX ${vx} is faulty"
				TMP_RESULT=NOK
			fi
		done

		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "VX Mounts are OK"
			TMP_RESULT=OK
		fi
	}
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash


TYPE="NASC"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        public_ethernet
        vx_mounts
done

