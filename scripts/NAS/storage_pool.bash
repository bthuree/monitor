#!/bin/bash

# emnas00001ts> storage pool list
# Pool                                       List of disks                             
# ==================================         ====================================      
# eniqsp1                                    emc_clariion0_100 
# oss1_pri                                   emc_clariion0_16 
# oss1_sec                                   emc_clariion0_17 
# smrssp1                                    emc_clariion0_77 

storage_pool() {
	ENIQ_POOL=0
	OSS_PRI=0
	OSS_SEC=0
	SMRS_POOL=0
	UNKNOWN=0

	## Send CHK_CMD command
	COMMAND="storage pool list"
	SSH_SERVER_ERR_2_NULL master | grep 'emc' |
	{
		while read output;
		do
			pool=$(echo $output | awk '{ print $1}' )
			disk=$(echo $output | awk '{ print $2}' )
			if [ "x${pool}" == "xeniqsp1" ]; then
				ENIQ_POOL=1
			elif [ "x${pool}" == "xoss1_pri" ] || [ "x${pool}" == "xoss1a_pri" ]; then
				OSS_PRI=1
			elif [ "x${pool}" == "xoss1_sec" ] || [ "x${pool}" == "xoss1a_sec" ]; then
				OSS_SEC=1
			elif [ "x${pool}" == "xsmrssp1"  ]; then
				SMRS_POOL=1
			else
				UNKNOWN=1
			fi
		done
	if [ $ENIQ_POOL -eq 1 -a $OSS_PRI -eq 1 -a $OSS_SEC -eq 1 -a $SMRS_POOL -eq 1 ]; then
			LOG_OK_RESULT "Storage pool ok"
	else
			LOG_FAIL_RESULT "Storage pool missing"
	fi		
	if [ $UNKNOWN -eq 1 ]; then
			LOG_OK_RESULT "Extra Storage pool found"
		fi
	}
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash


TYPE="NASC"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        storage_pool
done
