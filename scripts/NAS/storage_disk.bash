#!/bin/bash

# emnas00001ts> storage disk list
# Disk             emnas00001ts_02 emnas00001ts_01 
# ====             ========  ========  
# emc_clariion0_100 OK        OK        
# emc_clariion0_16 OK        OK        
# emc_clariion0_17 OK        OK        
# emc_clariion0_3  OK        OK        
# emc_clariion0_4  OK        OK        
# emc_clariion0_5  OK        OK        
# emc_clariion0_77 OK        OK        

storage_disks() {
	TMP_RESULT=OK

	## Send CHK_CMD command
	COMMAND="storage disk list"
	SSH_SERVER_ERR_2_NULL master | grep 'emc' |
	{
		while read output;
		do
			partition=$(echo $output | awk '{ print $1}' )
			ok1=$(echo $output | awk '{ print $2}' )
			ok2=$(echo $output | awk '{ print $3}' )
			if [ "${ok1}" != "OK" ] || [ "${ok2}" != "OK" ]; then
				LOG_FAIL_RESULT "NAS Disk ${partition} is faulty"
				TMP_RESULT=NOK
			fi
		done

		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "Storage Disks ok"
			TMP_RESULT=OK
		fi
	}
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash


TYPE="NASC"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        storage_disks
done

