#!/bin/bash

# emnas00001e2> storage fs list
# FS                        STATUS       SIZE    LAYOUT              MIRRORS   COLUMNS   USE%  NFS SHARED  CIFS SHARED  SECONDARY TIER
#   POOL LIST
# ========================= ======       ====    ======              =======   =======   ====  ==========  ===========  ==============  =========
# eniqsp1-admin             online      2.00G    simple              -         -           9%    yes          no           no           eniqsp1
# eniqsp1-admin-snss1       offline     2.00G    simple              -         -         0.0%     no          no           no            eniqsp1
# eniqsp1-sw                online      6.00G    simple              -         -          75%    yes          no           no            eniqsp1


storage_fs() {
	TMP_RESULT=OK

	## Send CHK_CMD command
	COMMAND="storage fs list"
	SSH_SERVER_ERR_2_NULL master | egrep -v "=====|POOL LIST" |
	{
		while read output;
		do
			partition=$(echo $output | awk '{ print $1}' )
			snapshot1=$(echo $partition | awk '/snss[0-9]/ {print $1}' )
			snapshot2=$(echo $partition | awk '/omss/ {print $1}' )
			snapshot3=$(echo $partition | awk '/_m[0-9]/ {print $1}' )
			snapshot=$(echo "${snapshot1}${snapshot2}${snapshot3}")
			status=$(echo $output | awk '{ print $2}' )
			used=$(echo $output | awk '{ print $7}' | cut -d'%' -f1 | cut -d'.' -f1 )
			if [ "x${snapshot}" == "x" ]; then
				if [ "${status}" != "online" ]; then
					LOG_FAIL_RESULT "Storage FS ${partition} is offline"
					TMP_RESULT=NOK
				fi
				if [ "$used" != "-" ]; then
					if [ $used -ge 98 ]; then
						LOG_FAIL_RESULT "Storage FS ${partition} is full (${used}%%)"
						TMP_RESULT=NOK
					fi
					if [ $used -ge 80 ]; then
						LOG_FAIL_RESULT "Storage FS ${partition} getting full (${used}%%)"
						TMP_RESULT=NOK
					fi
				fi
			fi
		done

		if [ "${TMP_RESULT}" == "OK" ]; then
			LOG_OK_RESULT "Storage FS disks ok"
			TMP_RESULT=OK
		fi
	}
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE="NASC"
SELECT_ENV

SERVERS=`SELECT_SERVER`
for SERVER in ${SERVERS}
do
        PING || continue
        storage_fs
done

