#!/bin/bash

ALLOWED_DISABLED="connTrackRes|nasgw_at|nasgw_db|nasgw_smweb"
ALLOWED_NOTPROBED="DNSAgent|nasgw_at|nasgw_db|nasgw_smweb"
ALLOWED_FROZEN="WEBSERVICE|TCPConnTrack"

storage_hastatus () {
        TMP_RESULT=OK                

        SERVER="${NAS_CONSOLE}"
        COMMAND="/opt/VRTS/bin/hastatus -sum"
        _OUTPUT="`SSH_SERVER_ERR_2_NULL`"

        _SRVS_OK=$(echo "$_OUTPUT" | grep "^A" | grep RUNNING | wc -l)

        _NFS_OK=$(echo "$_OUTPUT" | grep "^B" | grep ONLINE | awk '{print $2}' | grep "NFS$")
        _OSS_OK=$(echo "$_OUTPUT" | grep "^B" | grep ONLINE | awk '{print $2}' | grep "Oss$")

        _FROZEN_OK=$(echo "$_OUTPUT" | grep "^C" | egrep -v ${ALLOWED_FROZEN} | wc -l)
        _NOTPROBED_OK=$(echo "$_OUTPUT" | grep "^E" | egrep -v ${ALLOWED_NOTPROBED} | wc -l)
        _DISABLED_OK=$(echo "$_OUTPUT" | grep "^H" | egrep -v ${ALLOWED_DISABLED} | wc -l)

        if [ ${_SRVS_OK} -ne 2 ]; then
                LOG_FAIL_RESULT "At least one NAS server is not running"
                TMP_RESULT=NOK                
        fi

        if [ ${_FROZEN_OK} -gt 0 ]; then
                LOG_FAIL_RESULT "A NAS process is frozen"
                TMP_RESULT=NOK                
        fi
        if [ ${_NOTPROBED_OK} -gt 0 ]; then
                LOG_FAIL_RESULT "A NAS process is not probed"
                TMP_RESULT=NOK                
        fi
        if [ ${_DISABLED_OK} -gt 0 ]; then
                LOG_FAIL_RESULT "A NAS process is disabled"
                TMP_RESULT=NOK                
        fi

        if [ "${TMP_RESULT}" == "OK" ]; then
                LOG_OK_RESULT "NAS HA status looks ok"
        fi
}

_MYPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. ${_MYPATH}/../../lib/common.bash

TYPE=NASC
SELECT_ENV
NAS_CONSOLE=`SELECT_SERVER`


storage_hastatus
